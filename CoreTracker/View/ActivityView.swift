//
//  ActivityView.swift
//  MioGarage
//
//  Created by Salvatore Capuozzo on 18/11/2018.
//  Copyright © 2018 Salvatore Capuozzo. All rights reserved.
//

import UIKit

class ActivityView: UIView {
    struct Static {
        static var activityView = ActivityView()
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        self.initAll()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initAll() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        let width = window.frame.size.height
        let height = window.frame.size.height
        var squareLength: CGFloat
        var xOffset: CGFloat = 0
        var yOffset: CGFloat = 0
        if width > height {
            squareLength = width
            xOffset = -(width-height)/2
        } else {
            squareLength = height
            yOffset = -(height-width)/2
        }
        self.frame = CGRect(x: xOffset, y: yOffset, width: squareLength, height: squareLength)
        self.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = self.center
        loadingView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.style = UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(actInd)
        self.addSubview(loadingView)
        actInd.startAnimating()
    }
    
    static private func resetView() {
        Static.activityView.center = (UIApplication.shared.keyWindow?.center)!
    }
    
    static func show() {
        resetView()
        UIApplication.shared.keyWindow?.addSubview(Static.activityView)
    }
    
    static func remove() {
        Static.activityView.removeFromSuperview()
    }
    
    static func remove(withDelay delay: Double) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
            remove()
        }
    }
}
