//
//  SpeechSynthesizer.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 20/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import AVFoundation

class SpeechSynthesizer: AVSpeechSynthesizer {
    var rate: Float!
    var pitch: Float!
    var volume: Float!
    
    var totalUtterances: Int! = 0
    var currentUtterance: Int! = 0
    var totalTextLength: Int = 0
    var spokenTextLengths: Int = 0
    
    var preferredVoiceLanguageCode: String!
    var previousSelectedRange: NSRange!
    
    var textEnded: Bool = false
}
