//
//  SSRTextView.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 14/02/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import Foundation
import Speech

class SSRTextView: SSTextView, SFSpeechRecognizerDelegate {
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-GB"))!
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    private var isMicrophoneEnabled: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        isMicrophoneEnabled = false
        
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization {
            authStatus in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            @unknown default:
                print("Fatal error to handle")
            }
            
            OperationQueue.main.addOperation() {
                self.isMicrophoneEnabled = isButtonEnabled
            }
        }
    }
    
    func listenOrStop() {
        if audioEngine.isRunning {
            audioEngine.stop()
            stopRecording()
            recognitionRequest?.endAudio()
            isMicrophoneEnabled = false
        } else {
            startRecording()
        }
    }
    
    func changeLocale(languageID: String) {
        var identifier = ""
        switch languageID {
        case "fr":
            identifier = "fr-FR"
        case "it":
            identifier = "it-IT"
        case "en":
            identifier = "en-GB"
        case "es":
            identifier = "es-ES"
        case "ja":
            identifier = "ja-JA"
        case "de":
            identifier = "de-GE"
        default:
            print("Language not found")
            return
        }
        
        if let recognizer = SFSpeechRecognizer(locale: Locale.init(identifier: identifier)) {
            self.speechRecognizer = recognizer
        } else {
            print("The identifier is not valid")
        }
    }
    
    func getMicrophoneStatus() -> Bool {
        return isMicrophoneEnabled
    }
    
    func stopRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(false)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        do {
            try audioSession.setCategory(AVAudioSession.Category.soloAmbient, mode: AVAudioSession.Mode.measurement, options: [])
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
    }
    
    func startRecording() {
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: [])
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: {
            (result, error) in
            
            var isFinal = false
            
            if result != nil {
                
                self.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.isMicrophoneEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {
            (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        //self.text = "Say something, I'm listening!"
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            isMicrophoneEnabled = true
        } else {
            isMicrophoneEnabled = false
        }
    }
}
