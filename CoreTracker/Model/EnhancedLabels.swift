//
//  EnhancedLabels.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 19/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import Foundation

class EnhancedLabels {
    
    // The labels for the 80 classes.
    private static let frLabels = [
        "personne", "vélo", "voiture", "motocyclette", "avion", "bus", "train", "camion", "bateau", "feu de signalisation",
        "bouche d'incendie", "panneau stop", "parcmètre", "banc", "oiseau", "chat", "chein", "cheval", "mouton", "vache",
        "éléphant", "ours", "zèbre", "girafe", "sac à dos", "parapluie", "sac", "cravate", "valise", "frisbee",
        "skis", "snowboard", "ballon", "cerf-volant", "batte de base-ball", "gant de base-ball", "skateboard", "planche de surf", "raquette de tennis", "bouteille",
        "verre à vin", "tasse", "fourchette", "couteau", "cuillère", "bol", "banane", "pomme", "sandwich", "orange",
        "brocoli", "carotte", "hot dog", "pizza", "beignet", "gâteau", "chaise", "canapé", "jeune plant", "lit",
        "table", "toilettes", "écran", "portable", "souris", "télécommande", "clavier", "cellulaire", "micro-ondes", "four",
        "grille-pain", "lavabo", "frigo", "livre", "horloge", "vase", "ciseaux", "nounours", "sèche-cheveux", "brosse"
    ]
    private static let itLabels = [
        "persona", "bicicletta", "auto", "motocicletta", "aeroplano", "bus", "treno", "camion", "barca", "semaforo",
        "idrante", "segnale di stop", "parchimetro", "panchina", "uccello", "gatto", "cane", "cavallo", "pecora", "mucca",
        "elefante", "orso", "zebra", "giraffa", "zaino", "ombrello", "borsa", "cravatta", "valigia", "frisbee",
        "sci", "snowboard", "pallone", "aquilone", "mazza da baseball", "guanto da baseball", "skateboard", "tavola da surf", "racchetta da tennis", "bottiglia",
        "bicchiere da vino", "tazza", "forchetta", "coltello", "cucchiaio", "ciotola", "banana", "mela", "panino", "arancia",
        "broccoli", "carota", "hot dog", "pizza", "ciambella", "torta", "sedia", "divano", "piantina", "letto",
        "tavola", "gabinetto", "monitor tv", "portatile", "mouse", "telecomando", "tastiera", "cellulare", "microonde", "forno",
        "tostapane", "lavandino", "frigo", "libro", "orologio", "vaso", "forbici", "orsacchiotto", "asciugacapelli", "spazzolino"
    ]
    private static let enLabels = [
        "person", "bicycle", "car", "motorbike", "aeroplane", "bus", "train", "truck", "boat", "traffic light",
        "fire hydrant", "stop sign", "parking meter", "bench", "bird", "cat", "dog", "horse", "sheep", "cow",
        "elephant", "bear", "zebra", "giraffe", "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee",
        "skis", "snowboard", "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard", "tennis racket", "bottle",
        "wine glass", "cup", "fork", "knife", "spoon", "bowl", "banana", "apple", "sandwich", "orange",
        "broccoli", "carrot", "hot dog", "pizza", "donut", "cake", "chair", "sofa", "potted plant", "bed",
        "dining table", "toilet", "tv monitor", "laptop", "mouse", "remote", "keyboard", "cell phone", "microwave", "oven",
        "toaster", "sink", "refrigerator", "book", "clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush"
    ]
    private static let esLabels = [
        "persona", "bicicleta", "coche", "moto", "avión", "autobús", "tren", "camión", "barco", "semáforo",
        "boca de incendio", "señal de stop", "parquímetro", "banco", "pájaro", "gato", "perro", "caballo", "oveja", "vaca",
        "elefante", "oso", "cebra", "jirafa", "mochila", "paraguas", "bolso", "corbata", "maleta", "frisbee",
        "esquís", "snowboard", "pelota deportiva", "cometa", "bate de béisbol", "guante de béisbol", "patineta", "tabla de surf", "raqueta de tenis", "botella",
        "copa de vino", "copa", "tenedor", "cuchillo", "cuchara", "plato", "plátano", "manzana", "sándwich", "naranja",
        "brócoli", "zanahoria", "hot dog", "pizza", "donut", "pastel", "silla", "sofá", "pottedplant", "cama",
        "mesa de comedor", "inodoro", "tvmonitor", "computadora portátil", "mouse", "control remoto", "teclado", "teléfono celular", "horno de microondas", "horno",
        "tostadora", "fregadero", "refrigerador", "libro", "reloj", "florero", "tijeras", "oso de peluche", "secador de pelo", "cepillo de dientes"
    ]
    private static let deLabels = [
        "Person", "Fahrrad", "Auto", "Motorrad", "Flugzeug", "Bus", "Zug", "LKW", "Boot", "Ampel",
        "Hydrant", "Stoppschild", "Parkuhr", "Bank", "Vogel", "Katze", "Hund", "Pferd", "Schaf", "Kuh",
        "Elefant", "Bär", "Zebra", "Giraffe", "Rucksack", "Regenschirm", "Handtasche", "Krawatte", "Koffer", "Frisbee",
        "ski", "snowboard", "sportball", "kite", "baseballschläger", "baseballhandschuh", "skateboard", "surfbrett", "tennisschläger", "flasche",
        "Weinglas", "Tasse", "Gabel", "Messer", "Löffel", "Schüssel", "Banane", "Apfel", "Sandwich", "Orange",
        "Brokkoli", "Karotte", "Hot Dog", "Pizza", "Donut", "Kuchen", "Stuhl", "Sofa", "Topfpflanze", "Bett",
        "Esstisch", "Toilette", "TV-Monitor", "Laptop", "Maus", "Fernbedienung", "Tastatur", "Handy", "Mikrowelle", "Ofen",
        "Toaster", "Spüle", "Kühlschrank", "Buch", "Uhr", "Vase", "Schere", "Teddybär", "Haartrockner", "Zahnbürste"
    ]
    private static let jaLabels = [
        "人","自転車","車","バイク","飛行機","バス","電車","トラック","ボート","信号機",
        "消火栓","一時停止の標識","パーキングメーター","ベンチ","鳥","猫","犬","馬","羊","牛",
        "ゾウ","クマ","ゼブラ","キリン","バックパック","傘","ハンドバッグ","ネクタイ","スーツケース","フリスビー",
        "スキー","スノーボード","スポーツボール","カイト","野球用バット","野球用グローブ","スケートボード","サーフボード","テニスラケット","ボトル",
        "ワイングラス","カップ","フォーク","ナイフ","スプーン","ボール","バナナ","アップル","サンドイッチ","オレンジ",
        "ブロッコリー","にんじん","ホットドッグ","ピザ","ドーナツ","ケーキ","チェア","ソファー","鉢植え","ベッド",
        "食堂","トイレ","tvmonitor","ラップトップ","マウス","リモート","キーボード","携帯電話","電子レンジ","オーブン",
        "トースター","流し台","冷蔵庫","本","時計","花瓶","はさみ","テディベア","ヘアドライヤー","歯ブラシ"
    ]
    private static let ruLabels = [
        "человек", "велосипед", "автомобиль", "мотоцикл", "самолет", "автобус", "поезд", "грузовик", "лодка", "светофор",
        "пожарный гидрант", "знак остановки", "парковочный счетчик", "скамейка", "птица", "кошка", "собака", "лошадь", "овца", "корова",
        "слон", "медведь", "зебра", "жираф", "рюкзак", "зонтик", "сумочка", "галстук", "чемодан", "фрисби",
        "лыжи", "сноуборд", "спортивный мяч", "кайт", "бейсбольная бита", "бейсбольная перчатка", "скейтборд", "доска для серфинга", "теннисная ракетка", "бутылка",
        "бокал", "чашка", "вилка", "нож", "ложка", "миска", "банан", "яблоко", "сэндвич", "апельсин",
        "брокколи", "морковь", "хот-дог", "пицца", "пончик", "пирог", "стул", "диван", "горшечные растения", "кровать",
        "обеденный стол", "туалет", "твмонитор", "ноутбук", "мышь", "пульт", "клавиатура", "мобильный телефон", "микроволновка", "духовка",
        "тостер", "раковина", "холодильник", "книга", "часы", "ваза", "ножницы", "плюшевый мишка", "фен", "зубная щетка"
    ]
    private static let ptLabels = [
        "pessoa", "bicicleta", "automóvel", "avião", "autocarro", "autocarro", "camião", "barco", "semáforo",
        "boca de incêndio", "placa de pare", "parquímetro", "banco", "ave", "gato", "cachorro", "cavalo", "ovelha", "vaca",
        "elefante", "urso", "zebra", "girafa", "mochila", "guarda-chuva", "bolsa", "gravata", "mala", "frisbee",
        "esquis", "snowboard", "bola de desporto", "pipa", "taco de basebol", "luva de basebol", "skate", "prancha de surf", "raquete de ténis", "garrafa",
        "copo de vinho", "copo", "garfo", "faca", "colher", "tigela", "banana", "maçã", "sanduíche", "laranja",
        "brócolis", "cenoura", "cachorro-quente", "pizza", "donut", "bolo", "cadeira", "sofá", "pottedplant", "cama",
        "mesa de jantar", "banheiro", "tvmonitor", "laptop", "mouse", "remoto", "teclado", "celular", "microondas", "forno",
        "torradeira", "pia", "geladeira", "livro", "relógio", "vaso", "tesoura", "ursinho de pelúcia", "secador de cabelo", "escova de dentes"
    ]
    
    static func getLabels() -> [String] {
        switch LanguageHandler.getCurrentLanguage() {
        case .french:
            return frLabels
        case .italian:
            return itLabels
        case .english:
            return enLabels
        case .spanish:
            return esLabels
        case .german:
            return deLabels
        case .japanese:
            return jaLabels
        case .russian:
            return ruLabels
        case .portuguese:
            return ptLabels
        }
    }
}
