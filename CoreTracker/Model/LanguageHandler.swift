//
//  LanguageHandler.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 20/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import Foundation
import NaturalLanguage
import UIKit

class LanguageHandler {
    private static var selectedLanguage: Language = .english
    
    private static var nativeLanguage = Language.get(from: NSLocale.current.identifier)
    
    private static var languagesImageArray: [UIImage] = [#imageLiteral(resourceName: "france"), #imageLiteral(resourceName: "italy"), #imageLiteral(resourceName: "united-kingdom"), #imageLiteral(resourceName: "spain"), #imageLiteral(resourceName: "germany"), #imageLiteral(resourceName: "japan"), #imageLiteral(resourceName: "russia"), #imageLiteral(resourceName: "portugal")]
    private static var languagesArray: [String] = ["Français", "Italiano", "English", "Español", "Deutsch", "日本語", "Pусский язык", "Português"]
    
    static func getCurrentLanguage() -> Language {
        return selectedLanguage
    }
    
    static func getNativeLanguage() -> Language {
        return nativeLanguage
    }
    
    static func setLanguage(language: Language) {
        selectedLanguage = language
    }
    
    static func getLanguageImage(index: Int) -> UIImage {
        return languagesImageArray[index]
    }
    
    static func getLanguageName(index: Int) -> String {
        return languagesArray[index]
    }
    
    static func getLanguage(index: Int) -> Language {
        return Language.get(from: languagesArray[index])
    }
    
    static func getLanguagesCount() -> Int {
        return languagesArray.count
    }
    
}

enum Language {
    case french
    case italian
    case english
    case spanish
    case german
    case japanese
    case russian
    case portuguese
    
    static func get(from string: String) -> Language {
        switch string {
        case "French", "Français", "fr":
            return .french
        case "Italian", "Italiano", "it":
            return .italian
        case "English", "en":
            return .english
        case "Spanish", "Español", "es":
            return .spanish
        case "German", "Deutsch", "de":
            return .german
        case "Japanese", "日本語", "ja":
            return .japanese
        case "Russian", "Pусский язык", "ru":
            return .russian
        case "Portuguese", "Português", "pt":
            return .portuguese
        default:
            print("Not valid string, using english")
            return .english
        }
    }
    
    func getIdentifier() -> String {
        switch self {
        case .french:
            return "com.apple.ttsbundle.siri_female_fr-FR_compact"
        case .italian:
            return "com.apple.ttsbundle.Alice-compact"
        case .english:
            return "com.apple.ttsbundle.siri_female_en-GB-compact"
        case .spanish:
            return "com.apple.ttsbundle.Paulina-compact"
        case .german:
            return "com.apple.ttsbundle.siri_female_de-DE_compact"
        case .japanese:
            return "com.apple.ttsbundle.Kyoko-compact"
        case .russian:
            return "com.apple.ttsbundle.Milena-compact"
        case .portuguese:
            return "com.apple.ttsbundle.Joana-compact"
        }
    }
    
    func getIndex() -> Int {
        switch self {
        case .french:
            return 0
        case .italian:
            return 1
        case .english:
            return 2
        case .spanish:
            return 3
        case .german:
            return 4
        case .japanese:
            return 5
        case .russian:
            return 6
        case .portuguese:
            return 7
        }
    }
    
    func getConjuction() -> String {
        switch self {
        case .french:
            return " et "
        case .italian:
            return " e "
        case .english:
            return " and "
        case .spanish:
            return " y "
        case .german:
            return " und "
        case .japanese:
            return " と "
        case .russian:
            return " и "
        case .portuguese:
            return " e "
        }
    }
    
    func getShortIdentifier() -> String {
        switch self {
        case .french:
            return "fr"
        case .italian:
            return "it"
        case .english:
            return "en"
        case .spanish:
            return "es"
        case .german:
            return "de"
        case .japanese:
            return "ja"
        case .russian:
            return "ru"
        case .portuguese:
            return "pt"
        }
    }
    
    func getImage() -> UIImage {
        switch self {
        case .french:
            return #imageLiteral(resourceName: "france")
        case .italian:
            return #imageLiteral(resourceName: "italy")
        case .english:
            return #imageLiteral(resourceName: "united-kingdom")
        case .spanish:
            return #imageLiteral(resourceName: "spain")
        case .german:
            return #imageLiteral(resourceName: "germany")
        case .japanese:
            return #imageLiteral(resourceName: "japan")
        case .russian:
            return #imageLiteral(resourceName: "russia")
        case .portuguese:
            return #imageLiteral(resourceName: "portugal")
        }
    }
}

extension NLLanguage {
    func getShortIdentifier() -> String {
        switch self {
        case .french:
            return "fr"
        case .italian:
            return "it"
        case .english:
            return "en"
        case .spanish:
            return "es"
        case .german:
            return "de"
        case .japanese:
            return "ja"
        case .russian:
            return "ru"
        case .portuguese:
            return "pt"
        default:
            return "Still not implemented"
        }
    }
}
