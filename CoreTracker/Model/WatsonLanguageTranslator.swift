//
//  WatsonLanguageTranslator.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 02/02/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import Foundation
import LanguageTranslator

class WatsonLanguageTranslator {
    private static let translator = LanguageTranslator(version: "2019-01-01", apiKey: "UlecEEEmmgBp6MtVqIxM78QF4pGuswFu83RG6QK_WUqb")
    
    // Static function for text translation
    // The completion closure gives a string and two integers:
    // - The string is the result of the translation
    // - The first integer is the input index for the LanguageHandler
    // - The second integer is the output index for the LanguageHandler
    static func translate(text: String, sourceLanguage: Language? = nil, completion: @escaping (String, Int, Int) -> (Void)) {
        // Input Language Identification
        translator.identify(text: text) {
            (response, error) in
            if let er = error {
                print(er.errorDescription!)
                return
            }
            let source: String
            if let sourceLan = sourceLanguage {
                source = sourceLan.getShortIdentifier()
            } else {
                source = (response?.result?.languages.first?.language)!
                
            }
            let inputIndex = Language.get(from: source).getIndex()
            let target = LanguageHandler.getCurrentLanguage().getShortIdentifier()
            let outputIndex = Language.get(from: target).getIndex()
            print("\(source)-\(target)")
            
            if source != target {
                // Standard Translation
                self.translator.translate(text: [text], source: source, target: target) {
                    (response, error) in
                    if let er = error {
                        if er.errorDescription! == "Model not found." {
                            
                            // Bridge Translation (1: Soruce Language => English)
                            self.translator.translate(text: [text], source: source, target: "en") {
                                (response, error) in
                                if let er = error {
                                    print(er.errorDescription!)
                                    return
                                }
                                let englishText = (response?.result?.translations.first?.translationOutput)!
                                
                                // Bridge Translation (2: English => Target Language)
                                self.translator.translate(text: [englishText], source: "en", target: target) {
                                    (response, error) in
                                    if let er = error {
                                        print(er.errorDescription!)
                                        return
                                    }
                                    let translatedText = (response?.result?.translations.first?.translationOutput)!
                                    completion(translatedText, inputIndex, outputIndex)
                                }
                            }
                        } else {
                            print(er.errorDescription!)
                            return
                        }
                    } else {
                        if let translation = response?.result?.translations.first?.translationOutput {
                            let translatedText = translation
                            completion(translatedText, inputIndex, outputIndex)
                        }
                    }
                }
            }
        }
    }
}
