//
//  keras_inception_lstm_good+Fritz.swift
//  ShowAndTell
//

import Fritz

extension keras_inception_lstm_good: SwiftIdentifiedModel {

    // Check on Fritz Custom Account
    static let modelIdentifier = "0ba444902f6d4f3eb99afb9178db2094"

    static let packagedModelVersion = 1

    // Custom Key For This App (Fritz-Info.plist)
    static let session = Session(apiKey: "27738f52a82a4e96944b0be180c27de0")
}
