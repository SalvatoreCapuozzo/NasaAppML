//
//  CVPixelBuffer+Helpers.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 17/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import Foundation
import Accelerate
import Vision
import CoreImage

func resizePixelBuffer(_ srcPixelBuffer: CVPixelBuffer,
                       cropX: Int,
                       cropY: Int,
                       cropWidth: Int,
                       cropHeight: Int,
                       scaleWidth: Int,
                       scaleHeight: Int) -> CVPixelBuffer? {

  CVPixelBufferLockBaseAddress(srcPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
  guard let srcData = CVPixelBufferGetBaseAddress(srcPixelBuffer) else {
    print("Error: could not get pixel buffer base address")
    return nil
  }
  let srcBytesPerRow = CVPixelBufferGetBytesPerRow(srcPixelBuffer)
  let offset = cropY*srcBytesPerRow + cropX*4
  var srcBuffer = vImage_Buffer(data: srcData.advanced(by: offset),
                                height: vImagePixelCount(cropHeight),
                                width: vImagePixelCount(cropWidth),
                                rowBytes: srcBytesPerRow)

  let destBytesPerRow = scaleWidth*4
  guard let destData = malloc(scaleHeight*destBytesPerRow) else {
    print("Error: out of memory")
    return nil
  }
  var destBuffer = vImage_Buffer(data: destData,
                                 height: vImagePixelCount(scaleHeight),
                                 width: vImagePixelCount(scaleWidth),
                                 rowBytes: destBytesPerRow)

  let error = vImageScale_ARGB8888(&srcBuffer, &destBuffer, nil, vImage_Flags(0))
  CVPixelBufferUnlockBaseAddress(srcPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
  if error != kvImageNoError {
    print("Error:", error)
    free(destData)
    return nil
  }

  let releaseCallback: CVPixelBufferReleaseBytesCallback = { _, ptr in
    if let ptr = ptr {
      free(UnsafeMutableRawPointer(mutating: ptr))
    }
  }

  let pixelFormat = CVPixelBufferGetPixelFormatType(srcPixelBuffer)
  var dstPixelBuffer: CVPixelBuffer?
  let status = CVPixelBufferCreateWithBytes(nil, scaleWidth, scaleHeight,
                                            pixelFormat, destData,
                                            destBytesPerRow, releaseCallback,
                                            nil, nil, &dstPixelBuffer)
  if status != kCVReturnSuccess {
    print("Error: could not create new pixel buffer")
    free(destData)
    return nil
  }
  return dstPixelBuffer
}

func resizePixelBuffer(_ pixelBuffer: CVPixelBuffer,
                       width: Int, height: Int) -> CVPixelBuffer? {
  return resizePixelBuffer(pixelBuffer, cropX: 0, cropY: 0,
                           cropWidth: CVPixelBufferGetWidth(pixelBuffer),
                           cropHeight: CVPixelBufferGetHeight(pixelBuffer),
                           scaleWidth: width, scaleHeight: height)
}

// Complete and Show&Tell
extension CVPixelBuffer {
    func squareCropAtCenter(finalWidth: Int, finalHeight: Int) -> CIImage {
        let ciImage = CIImage(cvPixelBuffer: self)
        
        let width = Int(CVPixelBufferGetWidth(self))
        let height = Int(CVPixelBufferGetHeight(self))
        
        var cx = 0
        var cy = 0
        var cw = 0
        var ch = 0
        var tx = 0
        var ty = 0
        if width > height {
            cx = (width-height)/2
            cy = 0
            cw = height
            ch = height
            tx = -(width-height)/2
            ty = 0
        } else {
            cx = 0
            cy = (height-width)/2
            cw = width
            ch = width
            tx = 0
            ty = -(height-width)/2
        }
        let sw = CGFloat(finalWidth) / CGFloat(cw)
        let sh = CGFloat(finalHeight) / CGFloat(ch)
        
        let croppedImage = ciImage.cropped(to: CGRect(x: cx, y: cy, width: cw, height: ch))
        let movedImage = croppedImage.transformed(by: CGAffineTransform(translationX: CGFloat(tx), y: CGFloat(ty)))
        let scaleTransformation = CGAffineTransform(scaleX: sw, y: sh)
        let finalImage = movedImage.transformed(by: scaleTransformation)
        
        return finalImage
    }
    
    func squareScaleAtCenter(finalWidth: Int, finalHeight: Int) -> CIImage {
        let ciImage = CIImage(cvPixelBuffer: self)
        
        let sx = CGFloat(finalWidth) / CGFloat(CVPixelBufferGetWidth(self))
        let sy = CGFloat(finalHeight) / CGFloat(CVPixelBufferGetHeight(self))
        let scaleTransform = CGAffineTransform(scaleX: sx, y: sy)
        let scaledImage = ciImage.transformed(by: scaleTransform)
        
        return scaledImage
    }

}
