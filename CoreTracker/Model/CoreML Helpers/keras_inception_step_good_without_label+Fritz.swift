//
//  keras_inception_step_good_without_label+Fritz.swift
//  ShowAndTell
//

import Fritz

extension keras_inception_step_good_without_label: SwiftIdentifiedModel {

    // Check on Fritz Custom Account
    static let modelIdentifier = "c11bb2fb745040519c468476c7c0d34b"

    static let packagedModelVersion = 1

    // Custom Key For This App (Fritz-Info.plist)
    static let session = Session(apiKey: "27738f52a82a4e96944b0be180c27de0")
}
