//
//  SpeechTextViewController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 20/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import LanguageTranslator

class SpeechTextViewController: UIViewController, ContextualMenuDelegate, ContextualMenuDataSource {
    
    @IBOutlet var inputTextView: SSRTextView!
    @IBOutlet var outputTextView: SSTextView!
    
    @IBOutlet var inputBackView: UIView!
    @IBOutlet var outputBackView: UIView!
    
    @IBOutlet var inputLanguageImageView: UIImageView!
    @IBOutlet var outputLanguageImageView: UIImageView!
    
    @IBOutlet var microphoneButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var nextLanguageButton: UIButton!
    @IBOutlet var previousLanguageButton: UIButton!
    
    @IBOutlet var outputCopyButton: UIButton!
    
    var isPlaying: Bool = false
    var timer = Timer()
    
    var inputText: String = ""
    var outputText: String = ""
    var inputIndex: Int = 2
    var outputIndex: Int = LanguageHandler.getCurrentLanguage().getIndex()
    
    var outputLanguage: String = ""
    var translatedText: String = ""
    
    let mic = #imageLiteral(resourceName: "microphone").withRenderingMode(.alwaysTemplate)
    let play = #imageLiteral(resourceName: "play").withRenderingMode(.alwaysTemplate)
    let pause = #imageLiteral(resourceName: "pause").withRenderingMode(.alwaysTemplate)
    let stop = #imageLiteral(resourceName: "stop").withRenderingMode(.alwaysTemplate)
    let copy = #imageLiteral(resourceName: "copy").withRenderingMode(.alwaysTemplate)
    
    var isConnectionAvailable: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GradientTool.apply(colors: [
            CustomColor.lightBlue.uiColor(),
            CustomColor.middleBlue.uiColor(),
            CustomColor.classicBlue.uiColor()
            ], middlePos: 0.25, to: self.view)
        
        inputLanguageImageView.image = LanguageHandler.getLanguageImage(index: 2)
        outputLanguageImageView.image = LanguageHandler.getCurrentLanguage().getImage()
        
        inputBackView.layer.cornerRadius = 10
        inputBackView.layer.masksToBounds = true
        outputBackView.layer.cornerRadius = 10
        outputBackView.layer.masksToBounds = true
        outputBackView.backgroundColor = CustomColor.classicBlue.uiColor()
        
        inputTextView.font = UIFont(name: "Arial", size: 25)
        outputTextView.font = UIFont(name: "Arial", size: 25)
        outputTextView.backgroundColor = CustomColor.classicBlue.uiColor()
        outputTextView.textColor = .white
        
        microphoneButton.setImage(mic, for: .normal)
        microphoneButton.tintColor = .white
        
        playButton.setImage(play, for: .normal)
        playButton.tintColor = .white
        
        let prev = UIImage(cgImage: play.cgImage!, scale: 1.0, orientation: UIImage.Orientation.upMirrored).withRenderingMode(.alwaysTemplate)
        previousLanguageButton.setImage(prev, for: .normal)
        previousLanguageButton.tintColor = .white
        
        nextLanguageButton.setImage(play, for: .normal)
        nextLanguageButton.tintColor = .white
        
        outputCopyButton.setImage(copy, for: .normal)
        outputCopyButton.tintColor = .white
        
        let contextualMenu = ContextualMenu()
        contextualMenu.delegate = self
        contextualMenu.dataSource = self
        contextualMenu.menuType = .radial
        view.addSubview(contextualMenu)
        
        self.outputLanguage = LanguageHandler.getCurrentLanguage().getShortIdentifier()
        
        // Internet Connection Notification Center Setup
        NotificationCenter.default.addObserver(self, selector: #selector(self.statusManager), name: .flagsChanged, object: Network.reachability)
        checkCurrentReachability()
        
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(tryToStop), userInfo: nil, repeats: true)
        
        // Create a swipe down gesture for hiding the keyboard.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        view.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        outputIndex = LanguageHandler.getCurrentLanguage().getIndex()
        outputLanguageImageView.image = LanguageHandler.getLanguageImage(index: outputIndex)
        
        if !isConnectionAvailable {
            GSMessage.showMessageAddedTo("No internet connection, cannot translate", type: .warning, options: [], inView: self.view, inViewController: self)
        }
    }
    
    @objc func tryToStop() {
        if outputTextView.speechSynthesizer.textEnded {
            outputTextView.speechSynthesizer.textEnded = false
            outputTextView.textColor = .white
            playButton.setImage(play, for: .normal)
            playButton.tintColor = .white
            microphoneButton.setImage(mic, for: .normal)
            microphoneButton.tintColor = .white
            isPlaying = false
        }

        if isConnectionAvailable {
            outputBackView.backgroundColor = CustomColor.classicBlue.uiColor()
            outputTextView.backgroundColor = CustomColor.classicBlue.uiColor()
            if let text = self.inputTextView.text {
                if text != "" {
                    if self.inputText != self.inputTextView.text || self.outputLanguage != LanguageHandler.getCurrentLanguage().getShortIdentifier() {
                        var textToTranslate = text
                        if text == "Maria Luisa Farina" || text == "Maria Luisa farina" {
                            textToTranslate = "Un bellissimo e giovane coniglio d'affari"
                        } else if text == "Gloria Liliane Maria Cretella" {
                            textToTranslate = "L'amica che chiunque dovrebbe desiderare"
                        }
                        WatsonLanguageTranslator.translate(text: textToTranslate, completion: {
                            (result, input, output) in
                            self.outputText = result
                            self.inputIndex = input
                            self.outputIndex = output
                            self.inputText = text
                            self.outputLanguage = LanguageHandler.getCurrentLanguage().getShortIdentifier()
                        })
                    }
                    if self.inputIndex == self.outputIndex {
                        self.inputText = text
                        self.outputText = text
                    }
                }
            }
        } else {
            outputBackView.backgroundColor = CustomColor.fogGray.uiColor().withAlphaComponent(1.0)
            outputTextView.backgroundColor = CustomColor.fogGray.uiColor().withAlphaComponent(1.0)
            if self.inputIndex == self.outputIndex {
                self.inputText = self.inputTextView.text
                self.outputText = self.inputTextView.text
            }
        }
        
        self.outputTextView.text = self.outputText
        self.inputLanguageImageView.image = LanguageHandler.getLanguageImage(index: self.inputIndex)
        if outputLanguage == LanguageHandler.getCurrentLanguage().getShortIdentifier() || self.inputTextView.text == "" {
            ActivityView.remove()
        } else if inputIndex == outputIndex {
            ActivityView.remove()
        }
    }
    
    // iOS Contextual Menu
    func numberOfMenuItems(for menu: ContextualMenu) -> Int {
        return LanguageHandler.getLanguagesCount()
    }
    
    func contextualMenu(_ menu: ContextualMenu, viewForMenuItemAt index: Int) -> UIView {
        if !isConnectionAvailable {
            GSMessage.showMessageAddedTo("No internet connection, cannot translate", type: .warning, options: [], inView: self.view, inViewController: self)
            let empty = UIView()
            empty.backgroundColor = .clear
            return empty
        }
        let imageView = UIImageView(image: LanguageHandler.getLanguageImage(index: index))
        imageView.layer.cornerRadius = (imageView.image?.size.height)! / 2
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        return imageView
    }
    
    func contextualMenu(_ menu: ContextualMenu, didSelectItemAt index: Int) {
        if isConnectionAvailable {
            print(LanguageHandler.getLanguageName(index: index))
            LanguageHandler.setLanguage(language: Language.get(from: LanguageHandler.getLanguageName(index: index)))
            
            outputTextView.speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
            
            outputLanguageImageView.image = LanguageHandler.getLanguageImage(index: index)
            ActivityView.show()
        }
    }
    
    @objc func handleTapGesture(gestureRecognizer: UISwipeGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func microphonePressed(_ sender: Any) {
        if !isPlaying {
            microphoneButton.setImage(mic, for: .normal)
            // Touch feedback through vibration
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            if microphoneButton.tintColor == .orange {
                microphoneButton.tintColor = .white
            } else {
                microphoneButton.tintColor = .orange
            }
            inputTextView.listenOrStop()
        } else if isPlaying {
            outputTextView.stop()
            playButton.setImage(play, for: .normal)
            playButton.tintColor = .white
            microphoneButton.setImage(mic, for: .normal)
            isPlaying = false
        }
    }
    
    @IBAction func playPressed(_ sender: Any) {
        if !isPlaying {
            outputTextView.speak()
            playButton.setImage(pause, for: .normal)
            playButton.tintColor = .white
            microphoneButton.setImage(stop, for: .normal)
            microphoneButton.tintColor = .white
            isPlaying = true
        } else if isPlaying {
            outputTextView.pause()
            playButton.setImage(play, for: .normal)
            playButton.tintColor = .white
            microphoneButton.setImage(mic, for: .normal)
            microphoneButton.tintColor = .white
            isPlaying = false
        }
    }
    
    @IBAction func previousLanguagePressed(_ sender: Any) {
        var index = 0
        if self.inputIndex - 1 < 0 {
            index = LanguageHandler.getLanguagesCount() - 1
        } else {
            index = self.inputIndex - 1
        }
        self.inputIndex = index
        self.inputLanguageImageView.image = LanguageHandler.getLanguageImage(index: self.inputIndex)
        self.inputTextView.changeLocale(languageID: LanguageHandler.getLanguage(index: self.inputIndex).getShortIdentifier())
    }
    
    @IBAction func nextLanguagePressed(_ sender: Any) {
        self.inputIndex = (self.inputIndex + 1) % LanguageHandler.getLanguagesCount()
        self.inputLanguageImageView.image = LanguageHandler.getLanguageImage(index: self.inputIndex)
        self.inputTextView.changeLocale(languageID: LanguageHandler.getLanguage(index: self.inputIndex).getShortIdentifier())
    }
    
    @IBAction func outputCopyPressed(_ sender: Any) {
        UIPasteboard.general.string = self.outputTextView.text
        GSMessage.showMessageAddedTo("Copied", type: .info, options: nil, inView: self.view, inViewController: self)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
        } else {
            print("Portrait")
        }
        
        self.view.backgroundColor = CustomColor.middleBlue.uiColor()
        GradientTool.apply(colors: [
            CustomColor.middleBlue.uiColor(),
            CustomColor.middleBlue.uiColor(),
            CustomColor.middleBlue.uiColor()
            ], middlePos: 0.25, to: self.view)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
            GradientTool.apply(colors: [
                CustomColor.lightBlue.uiColor(),
                CustomColor.middleBlue.uiColor(),
                CustomColor.classicBlue.uiColor()
                ], middlePos: 0.25, to: self.view)
        }
    }
    
    // Added methods for reachability
    func checkCurrentReachability() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            print("-- Unreachable Connection --")
            isConnectionAvailable = false
        case .wifi:
            print("-- Connction Enabled - Wifi --")
            isConnectionAvailable = true
        case .wwan:
            print("-- Connection Enabled - Cellular --")
            isConnectionAvailable = true
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    
    @objc func statusManager(_ notification: NSNotification) {
        checkCurrentReachability()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
