//
//  OffTrCameraViewController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 17/02/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import CoreMedia
import AVFoundation

class OffTrCameraViewController: UIViewController, ContextualMenuDelegate, ContextualMenuDataSource {
    var videoCapture: VideoCapture!
    var boundingBoxes = [BoundingBox]()
    var colors: [UIColor] = []
    var predictionsRects: [(TrackerTools.Prediction, CGRect)] = []
    var framesDone = 0
    var frameCapturingStartTime = CACurrentMediaTime()
    let ciContext = CIContext()
    var startTimes: [CFTimeInterval] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTimeToLabel(text: "")
        
        setUpBoundingBoxes()
        setUpCoreImage()
        setUpVision()
        setUpCamera(completion: {})
        
        let contextualMenu = ContextualMenu()
        contextualMenu.delegate = self
        contextualMenu.dataSource = self
        contextualMenu.menuType = .radial
        view.addSubview(contextualMenu)
        
        frameCapturingStartTime = CACurrentMediaTime()
        
        setLanguageImageView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setLanguageImageView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        videoCapture.stop()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        videoCapture.stop()
    }
    
    // iOS Contextual Menu
    final func numberOfMenuItems(for menu: ContextualMenu) -> Int {
        return LanguageHandler.getLanguagesCount()
    }
    
    final func contextualMenu(_ menu: ContextualMenu, viewForMenuItemAt index: Int) -> UIView {
        let imageView = UIImageView(image: LanguageHandler.getLanguageImage(index: index))
        imageView.layer.cornerRadius = (imageView.image?.size.height)! / 2
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        return imageView
    }
    
    final func contextualMenu(_ menu: ContextualMenu, didSelectItemAt index: Int) {
        print(LanguageHandler.getLanguageName(index: index))
        LanguageHandler.setLanguage(language: Language.get(from: LanguageHandler.getLanguageName(index: index)))
        getSSTextView().speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
        setLanguageImageView()
    }
    
    final func resizePreviewLayer() {
        videoCapture.previewLayer?.frame = UIScreen.main.bounds
    }
    
    final func setUpCamera(completion: @escaping () -> ()) {
        videoCapture = VideoCapture()
        videoCapture.delegate = self
        videoCapture.desiredFrameRate = 240
        videoCapture.setUp(sessionPreset: AVCaptureSession.Preset.hd1920x1080) { success in
            if success {
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.addPreviewLayer(layer: previewLayer)
                    self.resizePreviewLayer()
                }
                
                // Add the bounding box layers to the UI, on top of the video preview.
                for box in self.boundingBoxes {
                    self.addBoxtoPreviewLayer(box: box)
                    //box.addToLayer(self.videoPreview.layer)
                }
                
                // Once everything is set up, we can start capturing live video.
                self.videoCapture.start()
                ActivityView.remove(withDelay: 1)
                completion()
            }
        }
    }
    
    final func show(predictions: [TrackerTools.Prediction]) {
        predictionsRects.removeAll()
        for i in 0..<boundingBoxes.count {
            if i < predictions.count {
                let prediction = predictions[i]
                
                // The predicted bounding box is in the coordinate space of the input
                // image, which is a square image of 416x416 pixels. We want to show it
                // on the video preview, which is as wide as the screen and has a 16:9
                // aspect ratio. The video preview also may be letterboxed at the top
                // and bottom.
                let width = view.bounds.width
                let height = view.bounds.height
                
                let scaleX = width / CGFloat(getBoxDimensions().width)
                let scaleY = height / CGFloat(getBoxDimensions().height)
                
                let top = (view.bounds.height - height) / 2
                
                // Translate and scale the rectangle to our own coordinate system.
                var rect = prediction.rect
                rect.origin.x *= scaleX
                rect.origin.y *= scaleY
                rect.origin.y += top
                rect.size.width *= scaleX
                rect.size.height *= scaleY
                
                predictionsRects.append((prediction, rect))
                
                // Show the bounding box.
                var label = String(format: "%@ %.1f", self.getLabels()[prediction.classIndex], prediction.score * 100)
                label.append("%")
                let color = colors[prediction.classIndex]
                boundingBoxes[i].show(frame: rect, label: label, color: color)
            } else {
                boundingBoxes[i].hide()
            }
        }
    }
    
    final func measureFPS() -> Double {
        // Measure how many frames were actually delivered per second.
        framesDone += 1
        let frameCapturingElapsed = CACurrentMediaTime() - frameCapturingStartTime
        let currentFPSDelivered = Double(framesDone) / frameCapturingElapsed
        if frameCapturingElapsed > 1 {
            framesDone = 0
            frameCapturingStartTime = CACurrentMediaTime()
        }
        return currentFPSDelivered
    }
    
    final func predict(image: UIImage) {
        if let pixelBuffer = image.pixelBuffer(width: getBoxDimensions().width, height: getBoxDimensions().height) {
            predict(pixelBuffer: pixelBuffer)
        }
    }
    
    internal func predict(pixelBuffer: CVPixelBuffer) {
        fatalError("This method must be overridden")
    }
    
    internal func getSSTextView() -> SSTextView {
        fatalError("This method must be overridden")
    }
    
    internal func getVideoPreview() -> UIView {
        fatalError("This method must be overridden")
    }
    
    internal func setLanguageImageView() {
        fatalError("This method must be overridden")
    }
    
    internal func setUpBoundingBoxes() {
        fatalError("This method must be overridden")
    }
    
    internal func addPreviewLayer(layer: AVCaptureVideoPreviewLayer) {
        fatalError("This method must be overridden")
    }
    
    internal func addBoxtoPreviewLayer(box: BoundingBox) {
        fatalError("This method must be overridden")
    }
    
    internal func getBoxDimensions() -> (width: Int, height: Int) {
        fatalError("This method must be overridden")
    }
    
    internal func setUpCoreImage() {
        fatalError("This method must be overridden")
    }
    
    internal func setUpVision() {
        fatalError("This method must be overridden")
    }
    
    internal func getLabels() -> [String] {
        fatalError("This method must be overridden")
    }
    
    internal func setTimeToLabel(text: String) {
        fatalError("This method must be overridden")
    }
    
    internal func videoCaptureTask(pixelBuffer: CVPixelBuffer?) {
        fatalError("This method must be overridden")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        resizePreviewLayer()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print(#function)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            if let vc = videoCapture {
                vc.previewLayer!.connection?.videoOrientation = .landscapeRight
                vc.videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .landscapeRight
            }
            
        } else {
            print("Portrait")
            if let vc = videoCapture {
                vc.previewLayer!.connection?.videoOrientation = .portrait
                vc.videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .portrait
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        guard let point = touch?.location(in: getVideoPreview()) else { return }
        var words: [String] = []
        for (prediction, rect) in predictionsRects {
            if rect.contains(point) {
                let label = String(format: "%@", getLabels()[prediction.classIndex])
                if !words.contains(label) {
                    words.append(label)
                    words.append(LanguageHandler.getCurrentLanguage().getConjuction())
                }
            }
        }
        if words.count != 0 {
            words.removeLast()
            var phrase: String = ""
            for word in words {
                phrase.append(word)
            }
            print(phrase)
            getSSTextView().text = phrase
            getSSTextView().speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
            getSSTextView().stop()
            getSSTextView().speak()
        }
    }
    
}

extension OffTrCameraViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        videoCaptureTask(pixelBuffer: pixelBuffer)
    }
}

protocol OffTrCVCProtocol {
    func predict(pixelBuffer: CVPixelBuffer)
    func getSSTextView() -> SSTextView
    func getVideoPreview() -> UIView
    func setLanguageImageView()
    func setUpBoundingBoxes()
    func addPreviewLayer(layer: AVCaptureVideoPreviewLayer)
    func addBoxtoPreviewLayer(box: BoundingBox)
    func getBoxDimensions() -> (width: Int, height: Int)
    func setUpCoreImage()
    func setUpVision()
    func getLabels() -> [String]
    func setTimeToLabel(text: String)
}
