//
//  TabBarController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 18/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        // Tab Bar Items
        // Enhanced
        let tabBarItem2 = self.tabBar.items![0]
        let cgEnhanced = #imageLiteral(resourceName: "enhanced").cgImage
        var scaleEnhanced: CGFloat = 0.0
        if UIScreen.main.nativeBounds.height > 2000 {
            scaleEnhanced = 28
        } else {
            scaleEnhanced = 22
        }
        let uiEnhanced = UIImage(cgImage: cgEnhanced!, scale: scaleEnhanced, orientation: .up)
        tabBarItem2.image = uiEnhanced.withRenderingMode(.alwaysTemplate)
        tabBarItem2.selectedImage = uiEnhanced.withRenderingMode(.alwaysTemplate)
        
        // Complete
        let tabBarItem3 = self.tabBar.items![1]
        let cgComplete = #imageLiteral(resourceName: "complete").cgImage
        var scaleComplete: CGFloat = 0.0
        if UIScreen.main.nativeBounds.height > 2000 {
            scaleComplete = 28
        } else {
            scaleComplete = 22
        }
        let uiComplete = UIImage(cgImage: cgComplete!, scale: scaleComplete, orientation: .up)
        tabBarItem3.image = uiComplete.withRenderingMode(.alwaysTemplate)
        tabBarItem3.selectedImage = uiComplete.withRenderingMode(.alwaysTemplate)
        
        // Speech&Text
        let tabBarItem5 = self.tabBar.items![2]
        let cgSpeechText = #imageLiteral(resourceName: "speech&Text").cgImage
        var scaleSpeechText: CGFloat = 0.0
        if UIScreen.main.nativeBounds.height > 2000 {
            scaleSpeechText = 28
        } else {
            scaleSpeechText = 22
        }
        let uiSpeechText = UIImage(cgImage: cgSpeechText!, scale: scaleSpeechText, orientation: .up)
        tabBarItem5.image = uiSpeechText.withRenderingMode(.alwaysTemplate)
        tabBarItem5.selectedImage = uiSpeechText.withRenderingMode(.alwaysTemplate)
        // Do any additional setup after loading the view.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        ActivityView.show()
        if let vc = viewController as? OffTrCameraViewController {
            tabBarController.tabBar.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                vc.setUpCamera(completion: {
                    tabBarController.tabBar.isUserInteractionEnabled = true
                })
            }
        }
        if let vc = viewController as? OnTrCameraViewController {
            tabBarController.tabBar.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                vc.setUpCamera(completion: {
                    tabBarController.tabBar.isUserInteractionEnabled = true
                })
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
