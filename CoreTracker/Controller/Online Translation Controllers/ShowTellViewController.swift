//
//  ShowTellViewController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 30/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import Vision
import AVFoundation
import CoreMedia

class ShowTellViewController: OnTrCameraViewController, OnTrCVCProtocol {
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var debugImageView: UIImageView!
    @IBOutlet var languageImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet var ssTextView: SSTextView!
    
    let showAndTell = ShowAndTell()
    
    var request: VNCoreMLRequest!
    
    let ciContext = CIContext()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print(#function)
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // OnTrCVCProtocol methods overriding
    override func getSSTextView() -> SSTextView {
        return self.ssTextView
    }
    
    override func addPreviewLayer(layer: AVCaptureVideoPreviewLayer) {
        self.videoPreview.layer.addSublayer(layer)
    }
    
    override func setLanguageImageView() {
        languageImageView.image = LanguageHandler.getCurrentLanguage().getImage()
    }
    
    override func addTapGestureToTextView() {
        textView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textViewTapped)))
    }
    
    override func getTextViewText() -> String {
        return self.textView.text
    }
    
    override func setTextViewText(text: String) {
        self.textView.text = text
    }

    @objc override func scanIteration() {
        //let startTime = Date()
        
        // Resize the input with Core Image to 299x299.
        guard let resizedPixelBuffer = resizedPixelBuffer else { return }
        guard let normalPixelBuffer = normalPixelBuffer else { return }
        
        let finalImage = normalPixelBuffer.squareCropAtCenter(finalWidth: 299, finalHeight: 299)
        ciContext.render(finalImage, to: resizedPixelBuffer)
        
        self.textView.text = nil

        let results = showAndTell.predict(pixelBuffer: resizedPixelBuffer, beamSize: 3, maxWordNumber: 30)
        //self.timeLabel.text = "Elapsed time：\(Date().timeIntervalSince(startTime) * 1000)ms"
        //GSMessage.showMessageAddedTo("Time elapsed：\(Date().timeIntervalSince(startTime) * 1000) ms", type: .info, options: nil, inView: self.view, inViewController: self)
        let predictionString = results.sorted(by: {$0.score > $1.score}).map({
            var x = $0.readAbleSentence.suffix($0.readAbleSentence.count - 1)
            if $0.sentence.last == Caption.endID {
                _ = x.removeLast()
            }
            var string = x.joined(separator: " ").capitalizingFirstLetter()
            string.removeLast(2)
            return String.init(format: "\(string) %.1f‱", pow(2, $0.score) * 10000.0)
        }).joined(separator: "\n\n")
        let firstPrediction = predictionString.lines.first!
        self.textView.text = firstPrediction
        
        self.translate(text: firstPrediction)
    }
    
    @objc override func textViewTapped() {
        var lines = textView.text.lines
        if lines.count > 0 {
            var line: String
            if LanguageHandler.getCurrentLanguage() != .english {
                line = lines[1]
            } else {
                line = lines[0]
            }
            var words = line.words
            if words.count > 0 {
                words.removeLast()
                ssTextView.text = ""
                for word in words {
                    ssTextView.text.append("\(word) ")
                }
                ssTextView.speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
                ssTextView.stop()
                ssTextView.speak()
            }
        }
    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    // MARK: - UI stuff
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        resizePreviewLayer()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        timer.invalidate()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(scanIteration), userInfo: nil, repeats: true)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            if let vc = videoCapture {
                vc.previewLayer!.connection?.videoOrientation = .landscapeRight
                vc.videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .landscapeRight
            }
            
        } else {
            print("Portrait")
            if let vc = videoCapture {
                vc.previewLayer!.connection?.videoOrientation = .portrait
                vc.videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .portrait
            }
        }
    }
}

extension String {
    func substring(_ from: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        return String(self[start ..< endIndex])
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension UIImage {
    /**
     Creates a new UIImage from an array of RGBA bytes.
     */
    @nonobjc public class func fromByteArrayRGBA(_ bytes: [UInt8],
                                                 width: Int,
                                                 height: Int,
                                                 scale: CGFloat = 0,
                                                 orientation: UIImage.Orientation = .up) -> UIImage? {
        return fromByteArray(bytes, width: width, height: height,
                             scale: scale, orientation: orientation,
                             bytesPerRow: width * 4,
                             colorSpace: CGColorSpaceCreateDeviceRGB(),
                             alphaInfo: .premultipliedLast)
    }
    
    /**
     Creates a new UIImage from an array of grayscale bytes.
     */
    @nonobjc public class func fromByteArrayGray(_ bytes: [UInt8],
                                                 width: Int,
                                                 height: Int,
                                                 scale: CGFloat = 0,
                                                 orientation: UIImage.Orientation = .up) -> UIImage? {
        return fromByteArray(bytes, width: width, height: height,
                             scale: scale, orientation: orientation,
                             bytesPerRow: width,
                             colorSpace: CGColorSpaceCreateDeviceGray(),
                             alphaInfo: .none)
    }
    
    @nonobjc class func fromByteArray(_ bytes: [UInt8],
                                      width: Int,
                                      height: Int,
                                      scale: CGFloat,
                                      orientation: UIImage.Orientation,
                                      bytesPerRow: Int,
                                      colorSpace: CGColorSpace,
                                      alphaInfo: CGImageAlphaInfo) -> UIImage? {
        var image: UIImage?
        bytes.withUnsafeBytes { ptr in
            if let context = CGContext(data: UnsafeMutableRawPointer(mutating: ptr.baseAddress!),
                                       width: width,
                                       height: height,
                                       bitsPerComponent: 8,
                                       bytesPerRow: bytesPerRow,
                                       space: colorSpace,
                                       bitmapInfo: alphaInfo.rawValue),
                let cgImage = context.makeImage() {
                image = UIImage(cgImage: cgImage, scale: scale, orientation: orientation)
            }
        }
        return image
    }
}
