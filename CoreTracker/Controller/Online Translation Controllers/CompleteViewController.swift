//
//  CompleteViewController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 16/02/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import Vision
import AVFoundation
import CoreMedia

class CompleteViewController: OnTrCameraViewController, OnTrCVCProtocol {
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var debugImageView: UIImageView!
    @IBOutlet var languageImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet var ssTextView: SSTextView!
    
    let ciContext = CIContext()

    let inceptionv3model = GoogLeNetPlaces()//MobileNet()//Inceptionv3()
    private var requests = [VNRequest]()
    
    var btTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVision()
        //let value = UIInterfaceOrientation.landscapeRight.rawValue
        //UIDevice.current.setValue(value, forKey: "orientation")
        
        
        
        textView.backgroundColor = .clear
    }
    /*
    override var shouldAutorotate: Bool {
        return true
    }*/
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btTimer.invalidate()
        //btTimer = Timer(timeInterval: 0.1, target: self, selector: #selector(btCall(_:)), userInfo: nil, repeats: true)
        videoPreview.layer.cornerRadius = 20
        videoPreview.layer.masksToBounds = true
    }
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // OnTrCVCProtocol methods overriding
    override func getSSTextView() -> SSTextView {
        return self.ssTextView
    }
    
    override func addPreviewLayer(layer: AVCaptureVideoPreviewLayer) {
        self.videoPreview.layer.addSublayer(layer)
    }
    
    override func setLanguageImageView() {
        languageImageView.image = LanguageHandler.getCurrentLanguage().getImage()
    }
    
    override func addTapGestureToTextView() {
        textView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textViewTapped)))
    }
    
    @objc func btCall(/*_ timer: Timer*/) {
        if UserDefaults.standard.bool(forKey: "deviceConnected") {
            self.sendToDevice(textToSend: "tempo", completion: {
                self.textView.text.append(self.receivedMessage)
                print(self.receivedMessage)
            })
        }
    }
    
    @objc override func textViewTapped() {
        var text = textView.text!
        if LanguageHandler.getCurrentLanguage() != .english {
            var lines = text.lines
            lines.removeFirst()
            text = ""
            for line in lines {
                text += line
            }
        }
        var words = text.words
        if words.count > 0 {
            words.removeLast()
            ssTextView.text = ""
            for word in words {
                ssTextView.text.append("\(word) ")
            }
            ssTextView.speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
            ssTextView.stop()
            ssTextView.speak()
            
            ssTextView.font = UIFont.systemFont(ofSize: 30)
        }
    }
    
    @objc override func scanIteration() {
        // Resize the input with Core Image to 299x299.
        guard let resizedPixelBuffer = resizedPixelBuffer else { return }
        guard let normalPixelBuffer = normalPixelBuffer else { return }
        
        let finalImage = normalPixelBuffer.squareCropAtCenter(finalWidth: 299, finalHeight: 299)
        ciContext.render(finalImage, to: resizedPixelBuffer)
        
        self.textView.text = nil
        
        self.handleImageBufferWithCoreML(pixelBuffer: resizedPixelBuffer)
    }
    
    override func getTextViewText() -> String {
        return self.textView.text
    }
    
    override func setTextViewText(text: String) {
        self.textView.text = text
    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    /*
    func handleImageBufferWithCoreML(imageBuffer: CMSampleBuffer) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(imageBuffer) else {
            return
        }
        do {
            let prediction = try self.inceptionv3model.prediction(image: self.resize(pixelBuffer: pixelBuffer)!)
            DispatchQueue.main.async {
                if let prob = prediction.classLabelProbs[prediction.classLabel] {
                    self.textView.text = "\(prediction.classLabel) \(String(describing: prob))"
                }
            }
        }
        catch let error as NSError {
            fatalError("Unexpected error ocurred: \(error.localizedDescription).")
        }
    }
    
    func handleImageBufferWithVision(imageBuffer: CMSampleBuffer) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(imageBuffer) else {
            return
        }
        
        var requestOptions:[VNImageOption : Any] = [:]
        
        if let cameraIntrinsicData = CMGetAttachment(imageBuffer, key: kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix, attachmentModeOut: nil) {
            requestOptions = [.cameraIntrinsics:cameraIntrinsicData]
        }
        
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, orientation: CGImagePropertyOrientation(rawValue: UInt32(self.exifOrientationFromDeviceOrientation))!, options: requestOptions)
        do {
            try imageRequestHandler.perform(self.requests)
        } catch {
            print(error)
        }
    }
    */
    private func handleImageBufferWithCoreML(pixelBuffer: CVPixelBuffer?) {
        if let pixelBuffer = pixelBuffer {
            do {
                let prediction = try self.inceptionv3model.prediction(sceneImage: self.resize(pixelBuffer: pixelBuffer)!)
                DispatchQueue.main.async {
                    if let prob = prediction.sceneLabelProbs[prediction.sceneLabel] {
                        let predictionString = "Environment: \(prediction.sceneLabel) (\(String(describing: round(prob*1000)/10))%)\n"
                        self.textView.text = predictionString
                        self.btCall()
                        //self.translate(text: predictionString)
                    }
                }
            }
            catch let error as NSError {
                fatalError("Unexpected error ocurred: \(error.localizedDescription).")
            }
        }
    }
    
    private func setupVision() {
        guard let visionModel = try? VNCoreMLModel(for: inceptionv3model.model) else {
            fatalError("can't load Vision ML model")
        }
        let classificationRequest = VNCoreMLRequest(model: visionModel) { (request: VNRequest, error: Error?) in
            guard let observations = request.results else {
                print("no results:\(error!)")
                return
            }
            
            let classifications = observations[0...4]
                .compactMap({ $0 as? VNClassificationObservation })
                .filter({ $0.confidence > 0.2 })
                .map({ "\($0.identifier) \($0.confidence)" })
            DispatchQueue.main.async {
                self.textView.text = classifications.joined(separator: "\n")
            }
        }
        classificationRequest.imageCropAndScaleOption = VNImageCropAndScaleOption.centerCrop
        
        self.requests = [classificationRequest]
    }
    
    
    /// only support back camera
    var exifOrientationFromDeviceOrientation: Int32 {
        let exifOrientation: DeviceOrientation
        enum DeviceOrientation: Int32 {
            case top0ColLeft = 1
            case top0ColRight = 2
            case bottom0ColRight = 3
            case bottom0ColLeft = 4
            case left0ColTop = 5
            case right0ColTop = 6
            case right0ColBottom = 7
            case left0ColBottom = 8
        }
        switch UIDevice.current.orientation {
        case .portraitUpsideDown:
            exifOrientation = .left0ColBottom
        case .landscapeLeft:
            exifOrientation = .top0ColLeft
        case .landscapeRight:
            exifOrientation = .bottom0ColRight
        default:
            exifOrientation = .right0ColTop
        }
        return exifOrientation.rawValue
    }
    
    /// resize CVPixelBuffer
    ///
    /// - Parameter pixelBuffer: CVPixelBuffer by camera output
    /// - Returns: CVPixelBuffer with size (299, 299)
    func resize(pixelBuffer: CVPixelBuffer) -> CVPixelBuffer? {
        let imageSide = 224//299
        var ciImage = CIImage(cvPixelBuffer: pixelBuffer, options: nil)
        //ciImage.oriented(forExifOrientation: 6)
        //let t = CGAffineTransform(rotationAngle: .pi/2)
        //ciImage = ciImage.transformed(by: t)
        let transform = CGAffineTransform(scaleX: CGFloat(imageSide) / CGFloat(CVPixelBufferGetWidth(pixelBuffer)), y: CGFloat(imageSide) / CGFloat(CVPixelBufferGetHeight(pixelBuffer)))
        ciImage = ciImage.transformed(by: transform).cropped(to: CGRect(x: 0, y: 0, width: imageSide, height: imageSide))
        
        let ciContext = CIContext()
        var resizeBuffer: CVPixelBuffer?
        CVPixelBufferCreate(kCFAllocatorDefault, imageSide, imageSide, CVPixelBufferGetPixelFormatType(pixelBuffer), nil, &resizeBuffer)
        ciContext.render(ciImage, to: resizeBuffer!)
        return resizeBuffer
    }
}
