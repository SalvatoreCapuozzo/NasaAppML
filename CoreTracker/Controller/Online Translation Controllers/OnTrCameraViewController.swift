//
//  OnTrCameraViewController.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 17/02/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import CoreMedia
import AVFoundation

class OnTrCameraViewController: AppViewController, ContextualMenuDelegate, ContextualMenuDataSource {
    var videoCapture: VideoCapture!
    var timer = Timer()
    var inputText: String = ""
    var originalText: String = ""
    var translatedText: String = ""
    var normalPixelBuffer: CVPixelBuffer?
    var resizedPixelBuffer: CVPixelBuffer?
    var isConnectionAvailable: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCoreImage()
        setUpCamera(completion: {})
        
        let contextualMenu = ContextualMenu()
        contextualMenu.delegate = self
        contextualMenu.dataSource = self
        contextualMenu.menuType = .radial
        view.addSubview(contextualMenu)
        
        timer.invalidate()
        
        // Internet Connection Notification Center Setup
        NotificationCenter.default.addObserver(self, selector: #selector(self.statusManager), name: .flagsChanged, object: Network.reachability)
        checkCurrentReachability()
        
        setLanguageImageView()
        
        addTapGestureToTextView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setLanguageImageView()
        
        if !isConnectionAvailable {
            GSMessage.showMessageAddedTo("No internet connection, cannot translate", type: .warning, options: [], inView: self.view, inViewController: self)
        }
        
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(scanIteration), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        videoCapture.stop()
        timer.invalidate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        videoCapture.stop()
    }
    
    // iOS Contextual Menu
    final func numberOfMenuItems(for menu: ContextualMenu) -> Int {
        return LanguageHandler.getLanguagesCount()
    }
    
    final func contextualMenu(_ menu: ContextualMenu, viewForMenuItemAt index: Int) -> UIView {
        checkCurrentReachability()
        if !isConnectionAvailable {
            GSMessage.showMessageAddedTo("No internet connection, cannot translate", type: .warning, options: [], inView: self.view, inViewController: self)
            let empty = UIView()
            empty.backgroundColor = .clear
            return empty
        }
        timer.invalidate()
        let imageView = UIImageView(image: LanguageHandler.getLanguageImage(index: index))
        imageView.layer.cornerRadius = (imageView.image?.size.height)! / 2
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        return imageView
    }
    
    final func contextualMenu(_ menu: ContextualMenu, didSelectItemAt index: Int) {
        if isConnectionAvailable {
            print(LanguageHandler.getLanguageName(index: index))
            LanguageHandler.setLanguage(language: Language.get(from: LanguageHandler.getLanguageName(index: index)))
            getSSTextView().speechSynthesizer.preferredVoiceLanguageCode = LanguageHandler.getCurrentLanguage().getIdentifier()
            setLanguageImageView()
            ActivityView.show()
        }
    }
    
    final func contextualMenuDidDismiss(_ menu: ContextualMenu) {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(scanIteration), userInfo: nil, repeats: true)
    }
    
    internal func translate(text: String) {
        // Translation
        var words: [String] = []
        for word in text.words {
            words.append(word)
        }
        words.removeLast()
        var myText = ""
        for word in words {
            myText.append("\(word) ")
        }
        myText.removeLast()
        
        if self.inputText != getTextViewText() && LanguageHandler.getCurrentLanguage() != .english {
            WatsonLanguageTranslator.translate(text: getTextViewText(), sourceLanguage: .english) {
                (result, input, output) -> (Void) in
                self.translatedText = result
                self.inputText = self.translatedText
                self.originalText = myText
            }
        }
        if self.translatedText != "" && LanguageHandler.getCurrentLanguage() != .english {
            ActivityView.remove(withDelay: 0.5)
            setTextViewText(text: "\(self.originalText):\n\(self.translatedText)")
        } else if LanguageHandler.getCurrentLanguage() == .english {
            ActivityView.remove(withDelay: 0.5)
        }
    }
    
    final func setUpCamera(completion: @escaping () -> ()) {
        videoCapture = VideoCapture()
        videoCapture.delegate = self
        videoCapture.desiredFrameRate = 240
        videoCapture.setUp(sessionPreset: AVCaptureSession.Preset.hd1920x1080) { success in
            if success {
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.addPreviewLayer(layer: previewLayer)
                    self.resizePreviewLayer()
                }
                
                // Once everything is set up, we can start capturing live video.
                self.videoCapture.start()
                ActivityView.remove(withDelay: 1)
                completion()
            }
        }
    }
    
    final func setUpCoreImage() {
        let status = CVPixelBufferCreate(nil, 299, 299,
                                         kCVPixelFormatType_32BGRA, nil,
                                         &resizedPixelBuffer)
        if status != kCVReturnSuccess {
            print("Error: could not create resized pixel buffer", status)
        }
    }
    
    final func resizePreviewLayer() {
        videoCapture.previewLayer?.frame = UIScreen.main.bounds
    }
    
    internal func setLanguageImageView() {
        fatalError("This method must be overridden")
    }
    
    @objc internal func textViewTapped() {
        fatalError("This method must be overridden")
    }
    
    @objc internal func scanIteration() {
        fatalError("This method must be overridden")
    }
    
    internal func getTextViewText() -> String {
        fatalError("This method must be overridden")
    }
    
    internal func setTextViewText(text: String) {
        fatalError("This method must be overridden")
    }
    
    internal func addTapGestureToTextView() {
        fatalError("This method must be overridden")
    }
    
    internal func addPreviewLayer(layer: AVCaptureVideoPreviewLayer) {
        fatalError("This method must be overridden")
    }
    
    internal func getSSTextView() -> SSTextView {
        fatalError("This method must be overridden")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Added methods for reachability
    func checkCurrentReachability() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            print("-- Unreachable Connection --")
            isConnectionAvailable = false
        case .wifi:
            print("-- Connction Enabled - Wifi --")
            isConnectionAvailable = true
        case .wwan:
            print("-- Connection Enabled - Cellular --")
            isConnectionAvailable = true
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    
    @objc func statusManager(_ notification: NSNotification) {
        checkCurrentReachability()
    }
}

extension OnTrCameraViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        // For debugging.
        //predict(image: UIImage(named: "dog416")!); return
        
        if let pixelBuffer = pixelBuffer {
            DispatchQueue.global().async {
                self.normalPixelBuffer = pixelBuffer
            }
        }
    }
}

protocol OnTrCVCProtocol {
    func setLanguageImageView()
    func textViewTapped()
    func scanIteration()
    func getTextViewText() -> String
    func setTextViewText(text: String)
    func addTapGestureToTextView()
    func addPreviewLayer(layer: AVCaptureVideoPreviewLayer)
    func getSSTextView() -> SSTextView
}
