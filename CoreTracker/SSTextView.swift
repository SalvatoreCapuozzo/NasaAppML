//
//  SSTextView.swift
//  CoreTracker
//
//  Created by Salvatore Capuozzo on 20/01/2019.
//  Copyright © 2019 Salvatore Capuozzo. All rights reserved.
//

import UIKit
import AVFoundation

class SSTextView: UITextView, AVSpeechSynthesizerDelegate {
    let speechSynthesizer = SpeechSynthesizer()
    
    struct ViewComponents {
        var pvSpeechProgress: UIProgressView!
    }
    
    var viewComponents: ViewComponents!
    
    var defaultTextColor: UIColor = .black
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        speechSynthesizer.rate = AVSpeechUtteranceDefaultSpeechRate
        speechSynthesizer.pitch = 1.0
        speechSynthesizer.volume = 1.0
        speechSynthesizer.preferredVoiceLanguageCode = "com.apple.ttsbundle.siri_female_en-GB-compact"
        speechSynthesizer.delegate = self
        self.text = ""
        setInitialFontAttribute(fontName: "Arial", textColor: .black)
    }
    
    func linkTo(pvSpeechProgress: UIProgressView) {
        viewComponents.pvSpeechProgress = pvSpeechProgress
    }
    
    func registerDefaultSettings() {
        speechSynthesizer.rate = AVSpeechUtteranceDefaultSpeechRate
        speechSynthesizer.pitch = 1.0
        speechSynthesizer.volume = 1.0
    }
    
    func setInitialFontAttribute(fontName: String, textColor: UIColor) {
        let rangeOfWholeText = NSMakeRange(0, self.text.utf16.count)
        let attributedText = NSMutableAttributedString(string: self.text)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: fontName, size: 38.0)!, range: rangeOfWholeText)
        self.textStorage.beginEditing()
        self.textStorage.replaceCharacters(in: rangeOfWholeText, with: attributedText)
        self.textStorage.endEditing()
        self.defaultTextColor = textColor
    }
    
    func speak() {
        if !speechSynthesizer.isSpeaking {
            /**
             let speechUtterance = AVSpeechUtterance(string: tvEditor.text)
             speechUtterance.rate = rate
             speechUtterance.pitchMultiplier = pitch
             speechUtterance.volume = volume
             speechSynthesizer.speakUtterance(speechUtterance)
             */
            
            let textParagraphs = self.text.components(separatedBy: "\n")
            
            speechSynthesizer.totalUtterances = textParagraphs.count
            speechSynthesizer.currentUtterance = 0
            speechSynthesizer.totalTextLength = 0
            speechSynthesizer.spokenTextLengths = 0
            
            for pieceOfText in textParagraphs {
                let speechUtterance = AVSpeechUtterance(string: pieceOfText)
                speechUtterance.rate = speechSynthesizer.rate
                speechUtterance.pitchMultiplier = speechSynthesizer.pitch
                speechUtterance.volume = speechSynthesizer.volume
                speechUtterance.postUtteranceDelay = 0.005
                
                if let voiceLanguageCode = speechSynthesizer.preferredVoiceLanguageCode {
                    let voice = AVSpeechSynthesisVoice(identifier: voiceLanguageCode)
                    speechUtterance.voice = voice
                }
                
                speechSynthesizer.totalTextLength = speechSynthesizer.totalTextLength + pieceOfText.utf16.count
                
                speechSynthesizer.speak(speechUtterance)
            }
        }
        else {
            speechSynthesizer.continueSpeaking()
        }
    }
    
    func pause() {
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
    }
    
    func stop() {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    // MARK: AVSpeechSynthesizerDelegate method implementation
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        speechSynthesizer.spokenTextLengths = speechSynthesizer.spokenTextLengths + utterance.speechString.utf16.count + 1
        
        if speechSynthesizer.totalTextLength != 0 {
            let progress: Float = Float(speechSynthesizer.spokenTextLengths * 100 / speechSynthesizer.totalTextLength)
            if let vc = viewComponents {
                vc.pvSpeechProgress.progress = progress / 100
            }
        } else {
            stop()
            speechSynthesizer.textEnded = true
        }
        
        if speechSynthesizer.currentUtterance == speechSynthesizer.totalUtterances {
            //animateActionButtonAppearance(shouldHideSpeakButton: false)
            
            unselectLastWord()
            speechSynthesizer.textEnded = true
            speechSynthesizer.previousSelectedRange = nil
        }
    }
    
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        speechSynthesizer.currentUtterance = speechSynthesizer.currentUtterance + 1
    }
    
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let progress: Float = Float(speechSynthesizer.spokenTextLengths + characterRange.location) * 100 / Float(speechSynthesizer.totalTextLength)
        if let vc = viewComponents {
            vc.pvSpeechProgress.progress = progress / 100
        }
        
        // Determine the current range in the whole text (all utterances), not just the current one.
        let rangeInTotalText = NSMakeRange(speechSynthesizer.spokenTextLengths + characterRange.location, characterRange.length)
        
        // Select the specified range in the textfield.
        self.selectedRange = rangeInTotalText
        
        // Store temporarily the current font attribute of the selected text.
        let currentAttributes = self.attributedText.attributes(at: rangeInTotalText.location, effectiveRange: nil)
        let fontAttribute: AnyObject? = currentAttributes[NSAttributedString.Key.font] as AnyObject
        
        // Assign the selected text to a mutable attributed string.
        let attributedString = NSMutableAttributedString(string: self.attributedText.attributedSubstring(from: rangeInTotalText).string)
        
        // Make the text of the selected area orange by specifying a new attribute.
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.orange, range: NSMakeRange(0, attributedString.length))
        
        // Make sure that the text will keep the original font by setting it as an attribute.
        attributedString.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, attributedString.string.utf16.count))
        
        // In case the selected word is not visible scroll a bit to fix this.
        self.scrollRangeToVisible(rangeInTotalText)
        
        // Begin editing the text storage.
        self.textStorage.beginEditing()
        
        // Replace the selected text with the new one having the orange color attribute.
        self.textStorage.replaceCharacters(in: rangeInTotalText, with: attributedString)
        
        // If there was another highlighted word previously (orange text color), then do exactly the same things as above and change the foreground color to black.
        if let previousRange = speechSynthesizer.previousSelectedRange {
            let previousAttributedText = NSMutableAttributedString(string: self.attributedText.attributedSubstring(from: previousRange).string)
            previousAttributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: defaultTextColor, range: NSMakeRange(0, previousAttributedText.length))
            previousAttributedText.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, previousAttributedText.length))
            
            self.textStorage.replaceCharacters(in: previousRange, with: previousAttributedText)
        }
        
        // End editing the text storage.
        self.textStorage.endEditing()
        
        // Keep the currently selected range so as to remove the orange text color next.
        speechSynthesizer.previousSelectedRange = rangeInTotalText
    }
    
    private func unselectLastWord() {
        if let selectedRange = speechSynthesizer.previousSelectedRange {
            // Get the attributes of the last selected attributed word.
            let currentAttributes = self.attributedText.attributes(at: selectedRange.location, effectiveRange: nil)
            // Keep the font attribute.
            let fontAttribute: AnyObject? = currentAttributes[NSAttributedString.Key.font] as AnyObject
            
            // Create a new mutable attributed string using the last selected word.
            let attributedWord = NSMutableAttributedString(string: self.attributedText.attributedSubstring(from: selectedRange).string)
            
            // Set the previous font attribute, and make the foreground color black.
            attributedWord.addAttribute(NSAttributedString.Key.foregroundColor, value: defaultTextColor, range: NSMakeRange(0, attributedWord.length))
            attributedWord.addAttribute(NSAttributedString.Key.font, value: fontAttribute!, range: NSMakeRange(0, attributedWord.length))
            
            // Update the text storage property and replace the last selected word with the new attributed string.
            self.textStorage.beginEditing()
            self.textStorage.replaceCharacters(in: selectedRange, with: attributedWord)
            self.textStorage.endEditing()
        }
    }
}

extension String {
    var lines: [String] { return self.components(separatedBy: NSCharacterSet.newlines) }
    var words: [String] { return self.components(separatedBy: " ")}
}
